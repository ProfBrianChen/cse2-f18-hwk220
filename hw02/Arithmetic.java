// CSE 002 hw 02
// Wendy Kim, September 8, 2018
// In this program, I'll be calculating the cost of items bought plus tax

public class Arithmetic {
  public static void main(String[] args) {
    
    int numPants = 3; // number of pairs of pants
    double pantsPrice = 34.98; // cost per pair of pants
    
    int numShirts = 2; // number of sweatshirts
    double shirtPrice = 24.99; // cost per sweatshirt
    
    int numBelts = 1; // number of belts
    double beltCost = 33.99; // cost of belt
    
    double paSalesTax = 0.06; // tax rate in PA
    
    double totalCostOfPants; // total cost of pants 
    double totalCostOfShirts; // total cost of sweatshirts
    double totalCostOfBelts; // total cost of belt
    double salesTaxOfPants; // tax charged on pants
    double salesTaxOfShirts; // tax charged on sweatshirts
    double salesTaxOfBelts; // tax charged on belts
    double totalCostBeforeTax; // total cost of all items before tax 
    double totalSalesTax; // sum of sales tax on all 3 items
    double totalCostPlusTax; // total cost of purchase plus tax
   
    totalCostOfPants = numPants * pantsPrice; // total cost of pants 
    totalCostOfShirts = numShirts * shirtPrice; // total cost of sweatshirts
    totalCostOfBelts = numBelts * beltCost; // total cost of belt
    totalCostBeforeTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; // total cost of purchase before tax
    salesTaxOfPants = (int)((totalCostOfPants * paSalesTax) * 100) / 100.0; // total tax charged on pants
    salesTaxOfShirts = (int)((totalCostOfShirts * paSalesTax) * 100) / 100.0; // total tax charged on sweatshirts
    salesTaxOfBelts = (int)((totalCostOfBelts * paSalesTax) * 100) / 100.0; // total tax charged on belts
    totalSalesTax = (int)((salesTaxOfPants + salesTaxOfShirts + salesTaxOfBelts) * 100) / 100.0;
    totalCostPlusTax = totalCostBeforeTax + totalSalesTax; // total cost of purchase + tax
    
    System.out.println("The total cost of pants is $" + totalCostOfPants);
    System.out.println("The total cost of sweatshirts is $" + totalCostOfShirts);
    System.out.println("The total cost of the belt is $" + totalCostOfBelts);
    
    System.out.println("The total tax charged on pants is $" + salesTaxOfPants);
    System.out.println("The total tax charged on sweatshirts is $" + salesTaxOfShirts);
    System.out.println("The total tax charged on the belt is $" + salesTaxOfBelts);
    
    System.out.println("The total cost of purchase before tax is $" + totalCostBeforeTax);
    System.out.println("The total sales tax charged on the purchase is $" + totalSalesTax);
    System.out.println("The total cost of purchase including tax is $" + totalCostPlusTax);
    
    
  } //end of main method
} //end of class