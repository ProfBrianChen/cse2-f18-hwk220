// CSE 002 lab 02
// Wendy (HyeJi) Kim, Sept. 7, 2018
// My bicycle cyclometer records two kinds of data:
//    the time elapsed in seconds
//    and the number of rotations of the front wheel during that time
// This program will print the number of minutes for each trip,
// the number of counts for each trip,
// the distance for each trip in miles,
// and the distance for the two trips combined

public class Cyclometer {
  // main method required for eavery Java program
  public static void main(String[] args) {
   
    int secsTrip1=480; // first trip took 480 seconds
    int secsTrip2=3220; // second trip took 3220 seconds
    int countsTrip1=1561; // first trip had 1561 front wheel rotations
    int countsTrip2=9037; // second trip had 9037 front wheel rotations
    double wheelDiameter=27.0, // diameter of the front wheel is 27 inches
    PI=3.14159, // pi
    feetPerMile=5280, // there are 5280 feet in 1 mile
    inchesPerFoot=12, // 12 inches in 1 foot
    secondsPerMinute=60; // 60 seconds in 1 minutes
    double distanceTrip1, distanceTrip2, totalDistance; // distance for trip 1, trip 2, and total distance
    
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " +
                      countsTrip1+" counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + "minutes and had " + 
                      countsTrip2+ " counts.");
    
    // this converts each trip from seconds to minutes (secsTrip/60 seconds)
    // and displays the number of front wheel counts 
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    // above gives distance in inches
    // (for each count, a rotation of the wheel travels
    // the diameter in inches times PI)
    distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance in miles
    distanceTrip2 = (countsTrip2 * wheelDiameter * PI) / inchesPerFoot / feetPerMile;
    // first find distance of Trip 2 in inches (# of counts * wheel diameter * pi)
    // and then convert inches to feet, and then feet to miles
    totalDistance = distanceTrip1 + distanceTrip2; // total distance of Trip 1 and 2
    
    // print out the output data
    System.out.println("Trip 1 was " + distanceTrip1 + " miles"); // prints distance in miles of Trip 1 
    System.out.println("Trip 2 was " + distanceTrip2 + " miles"); // prints distance in miles of Trip 2
    System.out.println("The total distance was " + totalDistance + " miles"); //combines distance of Trip 1 and 2
    
  } //end of main method
} //end of class

