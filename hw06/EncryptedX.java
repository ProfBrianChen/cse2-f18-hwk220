//CSE 002 hw 06: Encrypted X
//Wendy (HyeJi) Kim, October 19, 2018
//program to hide the secret message X
//ask user for an integer between 0-100 and validate input. This will be the size of the square
//measured by the number of stars.

import java.util.Scanner;
public class EncryptedX {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);

        System.out.print("Please input an integer between 0-100: ");
        
        int sizeSquare = 0;
        boolean intCheck;    
        
        //enter while loop to check if input is within range and if the input is an integer
        while(true){    
            intCheck = myScanner.hasNextInt();
            if(intCheck){   //if intCheck is true
                sizeSquare = myScanner.nextInt();  
                if((sizeSquare < 0) || (sizeSquare > 100)){ //if sizeSquare is out of range
                    System.out.print("Not within range. Please input an integer between 0-100: ");
                }
                    else{   
                        break;
                    }
            }
            else{   //if intCheck is not true
                myScanner.next();   //discards original input value
                System.out.print("Input not an integer. Please input an integer: ");
                intCheck = myScanner.hasNextInt();  //if user inputs an integer, assign to intCheck
                }     
        }

        //outer loop determines number of lines
        for(int row = 0; row <= sizeSquare; row++){     //row increases by 1 everytime inner loop finishes until it's equal to sizeSquare
            for(int column = 0; column < sizeSquare + 1; column++){  //position increases by 1 until position equals sizeSquare
                if((row == column) || (row == sizeSquare - column)){ 
                    System.out.print(" ");  //print a space for the first letter and last letter of first line; second letter and second to last letter of second line, etc.
                }    
                    else{
                        System.out.print("*");  //print * where there is no space printed
                    }
            }
            System.out.println(); //start each row with a new line
        }

    }
}