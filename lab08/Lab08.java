
import java.util.Scanner;
import java.util.Arrays;

public class Lab08{
    public static void main (String[] args){
        Scanner myScanner = new Scanner(System.in);
		int[] numbers = new int[100];

        //first set of arrays with 100 integers
        for(int i = 0; i < numbers.length; i++){
            numbers[i] = (int)(Math.random() * 100);
        }
        System.out.println("Array 1 holders the following integers: " + Arrays.toString(numbers));   //prints into an array
        System.out.println();
        
        //doesnt work because i would have to do a for loop for each number 0-99
        /*int counter = 0;
        for(int i = 0; i < numbers.length; i++){
            if(numbers[i] == 0){
                counter++;
            }
        }
        System.out.println("0 occurs " + counter + " times.");
        
        int counter1 = 0;
        for(int i = 0; i < numbers.length; i++){
            if(numbers[i] == 1){
                counter1++;
            }
        }
        System.out.println("1 occurs " + counter1 + " times.");*/
        int[] count = new int[100];
        int index = 0;

        //second set of array to store number of occurrences
        for(int i = 0; i < numbers.length; i++){
            index = numbers[i];     //index is the value at numbers[i]; checks every number in the array set
            count[index]++;         //increment for every occurrence 
        }

        //enter for loop to check for occurrences
        for(int i = 0; i < count.length; i++){
            if(count[i] > 0 && count[i] == 1){  //if the number of occurrence is 0 or 1
                System.out.println(i + " occurs " + count[i] + " time.");
            }
            else if(count[i] >= 2){ //if the number of occurrences is more than twice
                System.out.println(i + " occurs " + count[i] + " times.");
            }
        }

    } //end of main method
} //end of class