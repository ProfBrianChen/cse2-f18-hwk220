// CSE 002 lab 05: Reading erroneous inputs
// Wendy (HyeJi) Kim, October 5, 2018
// In this lab, I'll be writing a loop that asks the user to enter information
// relating to a course they are currently taking. I'll be asking for the course number,
// department name, number of times it meets a week, the time the class starts, the 
// instructor name, and the number of students.
// The program will also check that the user inputs the correct type. If the user does not
// input the correct type, an infinite loop will ask again. 
// Use next() to remove unwanted words when users do not type a word of the correct type. 
// It will print an error message saying it needs to input a correct type. 

import java.util.Scanner; //import Scanner

public class Erroneous{
  public static void main (String[] args){
    Scanner myScanner = new Scanner(System.in);
    
    //course number
    System.out.print("Input the course number: "); //ask user for course number
    int courseNumber; //declare courseNumber as an int
    boolean correctInt = myScanner.hasNextInt(); //checks if input is an int
    
    if (correctInt) { //if input is an integer 
      courseNumber = myScanner.nextInt(); //if input is an integer, assign user input to courseNumber 
    }
    else { //if input is not an integer 
      while (!correctInt) { // enter the while loop if input is not an integer
        myScanner.next(); //remove input if not integer
        System.out.print("Not an integer. Please input an integer: "); //asks user again for an integer input
          correctInt = myScanner.hasNextInt(); //if user inputs an integer, assign to correctInt
      }
      courseNumber = myScanner.nextInt(); // assigns to courseNumber if user inputs an integer
    }
    System.out.println("Your course number is " + courseNumber + "."); //print statement of course number
   
    //department name
    System.out.print("Input the department name: ");
    String deptName; 
    boolean correctString = myScanner.hasNext(); //checks if input is a String
    
    if (correctString) { //if input is a String, then assign input to deptName
      deptName = myScanner.next();
    }
    else{ //if input is not a String, enter the while loop
      while (!correctString) { //runs the while loop until input is the correct type
        myScanner.next();
        System.out.print("Not a string. Please input a string: ");
        correctString = myScanner.hasNext();
      }
      deptName = myScanner.next();
    }
    System.out.println("Your department name is " + deptName + ".");
    
    // number of meetings a week
    System.out.print("Input the number of times the course meets a week: ");
    int numbMeeting;
    correctInt = myScanner.hasNextInt();
    
    if(correctInt){
      numbMeeting = myScanner.nextInt();
    }
    else{
      while(!correctInt){
        myScanner.next();
        System.out.print("Not an integer. Please input an integer: ");
        correctInt = myScanner.hasNextInt();
      }
      numbMeeting = myScanner.nextInt();
    }
    System.out.println("The course meets " + numbMeeting + " times a week.");
    
    // class time
    System.out.print("Input the time the class starts: ");
    int timeClass;
    correctInt = myScanner.hasNextInt();

    if(correctInt){
      timeClass = myScanner.nextInt();
    }
    else{
      while(!correctInt){
        myScanner.next();
        System.out.print("Not an integer. Please input an integer: ");
        correctInt = myScanner.hasNextInt();
      }
      timeClass = myScanner.nextInt();
    }
    System.out.println("The course meets at " + timeClass + ".");

    
    // instructor name
    System.out.print("Input the name of your instructor: ");
    String instructorName;
    correctString = myScanner.hasNext();

    if(correctString){
      instructorName = myScanner.next();
    }
    else{
      while(!correctString){
        myScanner.next();
        System.out.print("Not a string. Please input a string: ");
        correctString = myScanner.hasNext();
      }
      instructorName = myScanner.next();
    }
    System.out.println("The name of your instructor is " + instructorName + ".");


    // number of students
    System.out.print("Input the number of students in the course: ");
    int numbStudents;
    correctInt = myScanner.hasNextInt();

    if(correctInt){
      numbStudents = myScanner.nextInt();
    }
    else{
      while(!correctInt){
        myScanner.next();
        System.out.print("Not an integer. Please input an integer: ");
        correctInt = myScanner.hasNextInt();
      }
      numbStudents = myScanner.nextInt();
    }
    System.out.println("There are " + numbStudents + " students in the course.");


    
  } //end of main method
} // end of class 