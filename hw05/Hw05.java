// CSE 002 Hw 05: While Loops
// Wendy (HyeJi) Kim, October 5, 2018
// The program will ask the user how many times it should generate hands.
// Each hand should generate 5 random numbers from 1-52.
// Cards 1-13 (diamonds), 14-26 (clubs), 27-39 (hearts), 40-52 (spades).
// Check if the set of 5 cards belong to 1 of the 4 hands listed in the hw assignment. 
// If it is, counter for the hand should increment by one.
// After generating all hands, calculate the probabilities of each of the 4 hands by computing
// the number of occurrences divided by the total number of hands
// Make sure no duplicate cards are generated in one hand.

import java.util.Scanner; //import Scanner

public class Hw05{
    public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); 

    double onePairCounter = 0;
    double twoPairCounter = 0;
    double threeOfKind = 0;
    double fourOfKind = 0;

    int randNum1 = (int)(Math.random()*52)+1; //random number between 1-52
    int randNum2 = 0;
    int randNum3 = 0;
    int randNum4 = 0;
    int randNum5 = 0;

    //ask user to input number of hands generated, assign to numHands
    System.out.print("Enter number of hands: ");
    int numHands = myScanner.nextInt();

    //generate 5 random numbers 
    for (int i = 1; i <= numHands; i++){
        randNum1 = (int)(Math.random()*52)+1;
        randNum2 = (int)(Math.random()*52)+1;
        randNum3 = (int)(Math.random()*52)+1;
        randNum4 = (int)(Math.random()*52)+1;
        randNum5 = (int)(Math.random()*52)+1;

        //enter while loop to check that there are no duplicate cards drawn
        while (randNum2 == randNum1){
            myScanner.nextLine();
            randNum2 = (int)(Math.random()*52)+1;
        }
        while((randNum3 == randNum2) && (randNum3 == randNum1)){
            myScanner.nextLine();
            randNum3 = (int)(Math.random()*52)+1;
        }
        while ((randNum4 == randNum3) && (randNum4 == randNum2) && (randNum4 == randNum1)){
            myScanner.nextLine();
            randNum4 = (int)(Math.random()*52)+1;
        }
        while ((randNum5 == randNum4) && (randNum5 == randNum3) && (randNum5 == randNum2) && (randNum5 == randNum1)){
            myScanner.nextLine();
            randNum5 = (int)(Math.random()*52)+1;
        }

        int card1 = 0;
        int card2 = 0;
        int card3 = 0;
        int card4 = 0;
        int card5 = 0;

        //use mod to convert numbers into between 1-13
        card1 = randNum1 % 13;
        card2 = randNum2 % 13;
        card3 = randNum3 % 13;
        card4 = randNum4 % 13;
        card5 = randNum5 % 13;

        //check for four of a kind
        if ((card1 == card2) && (card1 == card3) && (card1 == card4)){
            fourOfKind++;
        }
        else if ((card1 == card2) && (card1 == card3) && (card1 == card5)){
            fourOfKind++;
        }
        else if ((card1 == card3) && (card1 == card4) && (card1 == card5)){
            fourOfKind++;
        }
        else if ((card1 == card2) && (card1 == card4) && (card1 == card5)){
            fourOfKind++;
        }
        else if ((card2 == card3) && (card2 == card4) && (card2 == card5)){
            fourOfKind++;
        }
        //check for three of a kind
        else if ((card1 == card2) && (card1 == card3)){
            threeOfKind++;
        }
        else if ((card1 == card2) && (card1 == card4)){
            threeOfKind++;
        }
        else if ((card1 == card2) && (card1 == card5)){
            threeOfKind++;
        }
        else if ((card1 == card3) && (card1 == card4)){
            threeOfKind++;
        }
        else if ((card1 == card3) && (card1 == card5)){
            threeOfKind++;
        }
        else if ((card1 == card4) && (card1 == card5)){
            threeOfKind++;
        }
        else if ((card2 == card3) && (card2 == card4)){
            threeOfKind++;
        }
        else if ((card2 == card3) && (card2 == card5)){
            threeOfKind++;
        }
        else if ((card2 == card4) && (card2 == card5)){
            threeOfKind++;
        }
        else if ((card3 == card4) && (card3 == card5)){
            threeOfKind++;
        }
        //check for two pairs
        else if ((card1 == card2) && (card3 == card4)){
            twoPairCounter++;
        }
        else if ((card1 == card2) && (card3 == card5)){
            twoPairCounter++;
        }
        else if ((card1 == card2) && (card4 == card5)){
            twoPairCounter++;
        }
        else if ((card1 == card3) && (card2 == card4)){
            twoPairCounter++;
        }
        else if ((card1 == card3) && (card4 == card5)){
            twoPairCounter++;
        }
        else if ((card1 == card3) && (card2 == card5)){
            twoPairCounter++;
        }
        else if ((card1 == card4) && (card2 == card3)){
            twoPairCounter++;
        }
        else if ((card1 == card4) && (card2 == card5)){
            twoPairCounter++;
        }
        else if ((card1 == card4) && (card3 == card5)){
            twoPairCounter++;
        }
        else if ((card1 == card5) && (card2 == card3)){
            twoPairCounter++;
        }
        else if ((card1 == card5) && (card2 == card4)){
            twoPairCounter++;
        }
        else if ((card1 == card5) && (card3 == card4)){
            twoPairCounter++;
        }
        else if ((card2 == card3) && (card4 == card5)){
            twoPairCounter++;
        }
        else if ((card2 == card4) && (card3 == card5)){
            twoPairCounter++;
        }
        else if ((card2 == card5) && (card3 == card4)){
            twoPairCounter++;
        }

        //check for one pairs
        else if ((card1 == card2) || (card1 == card3) || (card1 == card4) || (card1 == card5) ||
            (card2 == card3) || (card2 == card4) || (card2 == card5) || 
            (card3 == card4) || (card3 == card5) || (card4 == card5)){
            onePairCounter++;
        }
        //System.out.println(card1 + " " + card2 + " " + card3 + " " + card4 + " " + card5);

    }
    double fourKindProb = (fourOfKind/numHands);
    double threeKindProb = (threeOfKind/numHands);
    double twoPairsProb = (twoPairCounter/numHands);
    double onePairProb = (onePairCounter/numHands);
    System.out.println(onePairCounter);
    System.out.println("The number of loops: " + numHands);
    System.out.println("The probability of Four-of-a-kind: ");
    System.out.printf("%.3f\n", fourKindProb);
    System.out.println("The probability of Three-of-a-kind: ");
    System.out.printf("%.3f\n", threeKindProb);
    System.out.println("The probability of Two-pairs: ");
    System.out.printf("%.3f\n", twoPairsProb);
    System.out.println("The probability of One-pair: ");
    System.out.printf("%.3f\n", onePairProb);

    } // end of main method
} // end of class