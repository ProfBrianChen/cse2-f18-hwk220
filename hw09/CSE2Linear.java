//CSE002 hw 09: Fun with searching
//Wendy (HyeJi) Kim, November 25, 2018
import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;

public class CSE2Linear{
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        int[] grades = new int[15];             

        System.out.println("Enter 15 ascending integers for final grades in CSE2: ");

        //go through each element of the array
        for(int i = 0; i < grades.length; i++){
            while (!scan.hasNextInt()){           //check if input is an integer
                scan.next();
                System.out.print("Input not an integer. Please enter an integer: ");
            }
            grades[i] = scan.nextInt();
            
            while (grades[i] < 0 || grades[i] > 100){       //check if input is within range 0-100
                System.out.print("Not within range. Please input an integer between 0-100: ");
                grades[i] = scan.nextInt();
            }
            if (i > 0){
                while(grades[i] < grades[i-1]){             //check if input is greater than the last input
                    System.out.print("Please input an integer greater than the last one entered: ");
                    grades[i] = scan.nextInt();
                }
            }
        } 
        System.out.println(Arrays.toString(grades));
        
        //binary search
        System.out.print("Enter a grade to search for: ");
        int key = scan.nextInt();
        int keyIndex = binarySearch(grades, key);
        if(keyIndex == -1){
            System.out.println(key + " was not found in the list.");
        }
        else{
            System.out.println("Found " + key + " at index " + keyIndex + ".");
        }

        //scrambled array
        System.out.println("Scrambled:");
        scramble(grades);

        //linear search
        System.out.print("Enter a grade to search for: ");
        int linearKey = scan.nextInt();
        int linearIndex = LinearSearch(grades, linearKey);
        
        if(linearIndex == -1){
            System.out.println(linearKey + " was not found.");
        }
        else {
            System.out.println("Found " + linearKey + " at index " + linearIndex + ".");
        }
        
    }

    public static int binarySearch(int[] list, int key){
        int low=0;
        int high = list.length-1;
        int mid;

        while(high >= low){             
            mid = (high + low) / 2;     //search the middle
            if(key > list[mid]){        //if key is greater than the middle value
                low = mid + 1;          //the new low is mid value plus 1
            }
            else if(key < list[mid]){   //if key is less than the mid value
                high = mid - 1;         //the new high is mid value minus 1
            }
            else{                      
                return mid;             //if key is equal to mid
            }                          
        }
        return -1;  //if not found
    }

    public static void scramble(int[] list){
        int temp;
        int random = (int)(Math.random() * (list.length));    
        for(int i = 0; i < list.length-1; i++){ 
            temp = list[i];
            list[i] = list[random];
            list[random] = temp;
        }
        System.out.println(Arrays.toString(list));
    }

    public static int LinearSearch(int[] list, int key){
        for(int i = 0; i < list.length; i++){
            if(list[i] == key){ 
                return i;
            }
        }
        return -1;   //if not found
    }
}