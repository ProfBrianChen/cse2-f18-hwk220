//CSE 002 hw 09: remove elements
//Wendy (HyeJi) Kim, November 26, 2018

import java.util.Scanner;
public class RemoveElements{
    public static void main(String [] arg){
        Scanner scan=new Scanner(System.in);
        int num[]=new int[10];
        int newArray1[];
        int newArray2[];
        int index,target;
	    String answer="";
	    do{
  	        System.out.println("Random input 10 ints [0-9] ");
  	        num = randomInput();
  	        String out = "The original array is: ";
  	        out += listArray(num);
  	        System.out.println(out);
 
  	        System.out.print("Enter the index: ");
  	        index = scan.nextInt();
  	        newArray1 = delete(num,index);
  	        String out1="The output array is: ";
  	        out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	        System.out.println(out1);
 
            System.out.print("Enter the target value: ");
  	        target = scan.nextInt();
  	        newArray2 = remove(num,target);
  	        String out2="The output array is: ";
  	        out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	        System.out.println(out2);
  	 
  	        System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	        answer=scan.next();
	    }
        while(answer.equals("Y") || answer.equals("y"));
    }
 
    public static String listArray(int num[]){
	    String out="{";
	    for(int j=0;j<num.length;j++){
  	        if(j>0){
    	        out+=", ";
  	        }
  	        out+=num[j];
	    }
	    out+="} ";
	    return out;
    }

    //generates an array of 10 random integers
    public static int[] randomInput(){
        int[] arrayList = new int[10];
        
        for(int i = 0; i < 10; i++){
            int random = (int)(Math.random() * 10);
            arrayList[i] = random;
        }
        return arrayList;
    }

    //delete value in index pos and generate a new array without that value
    public static int[] delete(int[] list, int pos){
        int[] array = new int[list.length-1];

        if(pos > 9 || pos < 0){
            System.out.println("The index is not within range. ");
            return list;
        }
        for(int i = 0; i < list.length-1; i++){
            if(i < pos){
                array[i] = list[i];
            }
            else if(i >= pos){
                array[i] = list[i+1];
            }
        }
        return array;
    }

    public static int[] remove(int[] list, int target){
        int counter = 0;

        //each time an element in the array equals the target, increment counter
        for(int i = 0; i < list.length; i++){
            if(list[i] == target){
                counter++;
            }
        }

        //if at least one instance occurs where element equals the target
        if(counter > 0){
            System.out.println("The target element was found.");
            int[] list2 = new int[list.length - counter];       //new list length is original list length minus counter
            int j = 0;

            //check if an element is equal to target
            for(int k = 0; k < list.length-counter; k++){
                if(list[k+j] == target){
                    ++j;
                }
                list2[k] = list[k+j];
            }
            return list2;
        }
        else{
            System.out.println("The target value is not in the array list.");
            return list;
        }



        
    }
}
