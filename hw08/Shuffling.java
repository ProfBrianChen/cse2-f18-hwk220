//CSE 002 Hw 08: Shuffling
//Wendy (HyeJi) Kim, November 12, 2018
//Shuffling a deck of cards, giving a hand of 5 cards
//first print out the cards in the deck, then shuffle the deck of cards and print them out shuffled,
//then get a hand of cards and print them out.

import java.util.Scanner;
public class Shuffling{ 
    public static void main(String[] args) { 
        Scanner scan = new Scanner(System.in); 
        //suits club, heart, spade or diamond 
        String[] suitNames={"C","H","S","D"};    
        String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
        String[] cards = new String[52]; 
        String[] hand = new String[5]; 
        int numCards = 5; 
        int again = 1; 
        int index = 51;
        for (int i=0; i<52; i++){ 
            cards[i]=rankNames[i%13]+suitNames[i/13]; 
        } 
        System.out.println();
        System.out.println("Sample Output: ");
        printArray(cards); 
        System.out.println();
        System.out.println("Shuffled: ");
        shuffle(cards); 
        printArray(cards); 
        while(again == 1){ 
            hand = getHand(cards,index,numCards); 
            System.out.println();
            System.out.println("Hand: ");
            printArray(hand);
            index = index - numCards; 
            //if statement to generate new shuffled deck once not enough cards are left in the index
            if(index < numCards){                   
                index = 51;
                shuffle(cards);                     //shuffle new deck
                System.out.println("New shuffled deck: ");
                printArray(cards);                  //print new shuffled deck
            }
            System.out.println("Enter a 1 if you want another hand drawn"); 
            again = scan.nextInt(); 
        }   
    }//end of main method

    //method to shuffle the original deck of cards
    public static void shuffle(String[] list){
        for(int i = 0; i < 70; i++){   //how many times to repeat (70 times)
            int j = (int)(Math.random() * (list.length-1)+1); 
            String swap = list[0];  
            list[0] = list[j];      //set index 0 equal to randomized number
            list[j] = swap;        
        }
    }

    //method to get hands consisting of 5 cards each
    public static String[] getHand(String[] list, int index, int numCards){
        String[] hand = new String[numCards];       //array hand that holds 5 spaces of numCards
        for(int i = 0; i < numCards; i++){
            hand[i] = list[index];  
            index--;                //decrement index so no duplicate values
        }
        return hand;
    }

    //method to print array
    public static void printArray(String[] list){
        for(int i= 0; i < list.length; i++){
            System.out.print(list[i] + " ");
        }
        System.out.println();
    }
  
}//end of class 
