//CSE 002 Hw 07: Word Tools
//Wendy (HyeJi) Kim, October 27, 2018
//manipulating strings and forcing the user to enter good input

import java.util.Scanner;

public class Hw07 {
    public static void main (String[] args){
        Scanner myScanner = new Scanner(System.in);
        String str = sampleText();      //store user inputted text into str
        System.out.println("You entered: " + str);
        char menu = 'a';
        menu = printMenu();     

        //while loop to check user intereped correct menu choice
        while (menu != 'q'){    
            if (menu == 'c'){   
                System.out.println("The number of non-whitespace characters: " + getNumOfNonWSCharacters(str));
                return;
            }
            else if (menu == 'w'){
                System.out.println("The number of words: " + getNumOfWords(shortenSpace(str)));
                //call shortenSpace method inside getNumOfWords method so the second space is not counted as a word
                return;
            }
            else if (menu == 'f'){
                System.out.println("Enter a word or phrase to be found: ");
                String find = myScanner.nextLine();
                System.out.println("\"" + find + "\" instances: " + findText(find, str));
                return;
            }
            else if (menu == 'r'){
                System.out.println("Edited text: " + replaceExclamation(str));
                return;
            }
            else if (menu == 's'){
                System.out.println("Edited text: " + shortenSpace(str));
                return;
            }
            else {
                System.out.println("Not one of the options in the menu. Please choose one of the options from above: ");
                menu = myScanner.next().charAt(0);
                }
            }
            System.out.println("Quit.");    
        }

    //user entered sample text method
    public static String sampleText(){
        Scanner myScanner = new Scanner(System.in);
        System.out.print("Please input a sample text: ");
        String text = myScanner.nextLine();
        return text;    //returns text (what the user inputted) to the main method
    }

    //method that prints out menu options
    public static char printMenu(){
        Scanner myScanner = new Scanner(System.in);
        System.out.println("The menu options are: ");
        System.out.println("c - Number of non-whitespace characters");
        System.out.println("w - Number of words");
        System.out.println("f - Find text");
        System.out.println("r - Replace all !'s");
        System.out.println("s - Shorten spaces");
        System.out.println("q - Quit");
        System.out.println();
        System.out.println("Please choose one of the options above: ");
        char menu = myScanner.next().charAt(0);
        
        return menu;    
    }

    //method to print number of non-whitespace characters
    public static int getNumOfNonWSCharacters(String nonWhiteSpaceChar){
        int length = nonWhiteSpaceChar.length();    //length of the string
        int count = 0;
        for (int i = 0; i < length; i++){
            //check for letters
            if (((nonWhiteSpaceChar.charAt(i) >= 'a') && (nonWhiteSpaceChar.charAt(i) <= 'z')) ||
                ((nonWhiteSpaceChar.charAt(i) >= 'A') && (nonWhiteSpaceChar.charAt(i) <= 'Z'))){
                count++;
            }
            //check for punctuations
            else if (nonWhiteSpaceChar.charAt(i) == ',' || nonWhiteSpaceChar.charAt(i) == '.' ||
             nonWhiteSpaceChar.charAt(i) == '!' || nonWhiteSpaceChar.charAt(i) == ';' || 
             nonWhiteSpaceChar.charAt(i) == ':' || nonWhiteSpaceChar.charAt(i) == '\'') { 
                 count++;
            }
        }
        return count;   //returns the number of characters(without whitespace) to main method
    }

    //method to count number of words 
    public static int getNumOfWords(String numWords){
        int length = numWords.length();
        int count = 0;
    
        for (int i = 0; i < length; i++){
            if (numWords.charAt(i) == ' '){     //count whenever a space is reached
                count++;
            }
        }
        return count+1;     //count+1 because last word doesn't have a whitespace after
    }

    //method to return number of instances a word/phrase is found
    public static int findText(String textToFind, String find){
        System.out.print("Enter a word or phrase to be found: ");
        //text = myScanner.nextLine();
        int instances = 0;
        for (int i = 0; i <= (find.length()-textToFind.length()); i++){
            if(find.substring(i, i + textToFind.length()).equals(textToFind)){
                instances++;
            }
        }
        return instances;
    }

    //method to replace exclamation marks with a period
    public static String replaceExclamation(String text){
        String replace = "";
        int length = text.length();
        for (int i = 0; i < length; i++){
            if (text.charAt(i) == '!'){
                replace = replace + '.';
            }
            else {
                replace = replace + text.charAt(i);
            }
        }
        return replace;     //returns user inputted text with periods in replace of exclamation marks
    }

    //method to shorten 2 or more whitespaces in a string
    public static String shortenSpace(String numSpace){ //help what if it's 3 spaces
        String replaceSpaces = "";
        int length = numSpace.length();
        for (int i = 0; i < length; i++){
            if ((numSpace.charAt(i) == ' ') && (numSpace.charAt(i+1) == ' ')){
                replaceSpaces = numSpace.replace("  ", " ");  //replace() method with parameters 2 spaces, replaced with 1 space
            }
        }
        return replaceSpaces;     //returns user inputted text with correct amount of spaces
    }
}