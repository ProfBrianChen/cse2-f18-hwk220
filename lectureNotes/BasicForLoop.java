import java.util.Scanner;

public class basicForLoop{
	public static void main(String[] args){
	
		//while (boolean condition is true), excecute the body of the loop:
		// Print all integers from 0 to 10
		for (int j = 0; j < 10; j++){
			System.out.printf("The variable j = %d.\n", j);
		}
		//System.out.printf("j terminated at value %d.", j);
		// Is it possible to program an infinite for loop?
		//int i = 0;
		//for (int j = 0; i < 10; j++){
		//	System.out.printf("The variable j = %d.\n", j);
		//}
	}
	
}