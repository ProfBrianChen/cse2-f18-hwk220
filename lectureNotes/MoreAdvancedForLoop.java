import java.util.Scanner;

// Later, we'll discuss how to use arrays;
// for now, we'll just note that we need to
// import the Array class to use its methods
import java.util.Arrays;

public class MoreAdvancedForLoop{
  
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);  
    System.out.println("Input number of students in class today: ");    
    int numStudents = myScanner.nextInt();    
    int myClassArray[] = new int[numStudents];

    // Why set age to -1?
    int age = -1;
		
		for(int counter = 1; counter < numStudents + 1; counter++){
      System.out.printf("Input student %d age: ", counter);
      age = myScanner.nextInt();
      myClassArray[counter - 1] = age;
      if (age < 18){
        System.out.printf("Student %d is under 18, appropriate FERPA paperwork must be filed. Please exclude this student from further data collection until such paperwork has been filed.\n", counter);
        break;
      }
    }
    
    // The following prints the entire array to the screen
    System.out.println(Arrays.toString(myClassArray));
    
  }
  
}