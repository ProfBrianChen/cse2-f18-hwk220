import java.util.Scanner;

public class switchDemo{
	public static void main(String[] args){
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Input the number of classes you have taken at Lehigh.");
		int numberOfClasses = myScanner.nextInt();
		
		if (numberOfClasses == 1){
			System.out.println("You have taken 1 class.");
		}
		else if (numberOfClasses == 2){
			System.out.println("You have taken 2 classes.");
		}
		else if (numberOfClasses == 3){
			System.out.println("You have taken 3 classes.");
		}
		else if (numberOfClasses == 4){
			System.out.println("You have taken 4 classes.");
		}
		else if (numberOfClasses == 5){
			System.out.println("You have taken 5 classes.");
		}
		else if (numberOfClasses == 6){
			System.out.println("You have taken 6 classes.");
		}
		else if (numberOfClasses == 7){
			System.out.println("You have taken 7 classes.");
		}
		else if (numberOfClasses == 8){
			System.out.println("You have taken 8 classes.");
		}
		else if (numberOfClasses == 9){
			System.out.println("You have taken 9 classes.");
		}
		else if (numberOfClasses == 10){
			System.out.println("You have taken 10 classes.");
		}//Have I made a point yet?
		
		myScanner.nextLine();
		
		//Alternatively, we can use switch statements:
		System.out.println("Enter the month you were born (as an integer).");
		int month = myScanner.nextInt();
    String monthString;
    switch (month) {
			case 1:  monthString = "January";
			case 2:  monthString = "February";
			case 3:  monthString = "March";
				break;
			case 4:  monthString = "April";
				break;
			case 5:  monthString = "May";
				break;
			case 6:  monthString = "June";
				break;
			case 7:  monthString = "July";
				break;
			case 8:  monthString = "August";
				break;
			case 9:  monthString = "September";
				break;
			case 10: monthString = "October";
				break;
			case 11: monthString = "November";
				break;
			case 12: monthString = "December";
				break;
			default: monthString = "Invalid month";
        break;
		}
		System.out.println(monthString);
		
		myScanner.nextLine();
		// Using something other than numbers as input
		System.out.println("Enter your letter grade in this class.");
		char grade = myScanner.next().charAt(0);
      switch(grade) {
         case 'A' :
            System.out.println("Excellent!"); 
            break;
         case 'B' :
         case 'C' :
            System.out.println("Well done");
            break;
         case 'D' :
            System.out.println("You passed");
         case 'F' :
            System.out.println("Better try again");
            break;
         default :
            System.out.println("Invalid grade");
      }
    System.out.println("Your grade is " + grade + ".");
		
		//myScanner.nextLine();
		
		//myScanner.nextLine();
		
		//System.out.println("When printing, we often like to impose some formatting.  For example:");
		//double printDouble1 = 5.0 + 1.0/3.0;
		//System.out.printf("5 1/3 as a decimal can be written: %.9f\n", printDouble1);
		//myScanner.nextLine();
		//System.out.printf("5 1/3 as a decimal can also be written: %.5f\n", printDouble1);
		//myScanner.nextLine();

	}
	
}