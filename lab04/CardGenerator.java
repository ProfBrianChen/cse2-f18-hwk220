//CSE002 lab 04
//Wendy (HyeJi) Kim, September 21, 2018
//In this lab, I'll be writing a program that'll allow me to randomly generate
//a number from 1-52 inclusive. Each number will represent one card.
//Cards 1-13 represent diamonds, 14-26 represent clubs, 27-39 represent hearts, and 40-52 represent spades.
//Each card identity ascends in step with the card number.

public class CardGenerator {
  //main method required for every Java program
  public static void main(String[] args) {
    
    int randCard = (int)(Math.random()*52)+1;  //declare randCard as int
    //Math.random generates a random number 
    //multiply by 52 because there are 52 cards
    //plus one so it starts at 1
    String suit = "xyz"; //declare suit as a String
    String randomNum = "xyz"; //declare randomNum as a String to use for print statement 
    int mod = randCard % 13; //declare mod as an int 
    //mod 13 because there are 13 cards in each suit, so remainder will be between 0-12
    //by using the modulus function, the remainder and hence the random number will only be between 0-12, 0 being King
    //so instead of using numbers 1-52, only use 0-12 for the switch command
    //makes program shorter-- don't have to write 52 cases for each number for the switch statement  
    
    if ((randCard >= 1) && (randCard <= 13)) { 
      suit="Diamonds"; //if randCard is between 1-13, assign the suit Diamonds 
    }
      if ((randCard >= 14) && (randCard <= 26)) {
        suit="Clubs"; //if randCard is between 14-26, assign the suit Clubs
      }  
      if ((randCard >=27) && (randCard <= 39)) {
        suit="Hearts"; //if randCard is between 27-39, assign the suit Hearts
      }
      if ((randCard >= 40) && (randCard <= 52)) {
        suit="Spades"; //if randCard is between 40-52, assign the suit Spades
      } 

    
    switch(mod) { //switch statements to assign randomNum to a card number or suit 
      case 1: randomNum = "Ace"; //if mod (remainder) is 1, assign randomNum as Ace
        break;
      case 2: randomNum = "2"; //if mod (remainder) is 2, assign randomNum as 2
        break;
      case 3: randomNum = "3"; //if mod (remainder) is 3, assign randomNum as 3
        break;
      case 4: randomNum = "4"; //if mod (remainder) is 4, assign randomNum as 4
        break;
      case 5: randomNum = "5"; //if mod (remainder) is 5, assign randomNum as 5
        break;
      case 6: randomNum = "6"; //if mod (remainder) is 6, assign randomNum as 6
        break;
      case 7: randomNum = "7"; //if mod (remainder) is 7, assign randomNum as 7
        break;
      case 8: randomNum = "8"; //if mod (remainder) is 8, assign randomNum as 8
        break;
      case 9: randomNum = "9"; //if mod (remainder) is 9, assign randomNum as 9
        break;
      case 10: randomNum = "10"; //if mod (remainder) is 10, assign randomNum as 10
        break;
      case 11: randomNum = "Jack"; //if mod (remainder) is 11, assign randomNum as Jack
        break;
      case 12: randomNum = "Queen"; //if mod (remainder) is 12, assign randomNum as Queen
        break;
      case 0: randomNum = "King"; //if mod (remainder) is 0, assign randomNum as King
        break;
    }
    
  System.out.println("You picked the " + randomNum + " of " + suit); //prints out the randomly selected card 
    
    
  }
}