// CSE 002 hw 03- Pyramid
//September 16, 2018
//Wendy (HyeJi) Kim
//In this program, I'll be asking the user for the dimensions of a pyramid
//and use the information to find the volume inside the pyramid. 

import java.util.Scanner; //import Scanner class

public class Pyramid{
  //main method required for every Java program
  public static void main (String[] args) {
    
    Scanner myScanner = new Scanner(System.in); //construct instance of Scanner
    
    System.out.print("The square side of the pyramid is (input length): "); //asks user for length of the square side of the pyramid
    //since it's a square, accounts for both length and width
    double pyramidLength = myScanner.nextDouble(); //accepts user input and assigns the value to pyramidLength
    
    System.out.print("The height of the pyramid is (input height): "); //asks user for the height of the pyramid
    double pyramidHeight = myScanner.nextDouble(); //accepts user input and assigns the value to pyramidHeight
    
    double pyramidVolume = ((pyramidLength * pyramidLength) * pyramidHeight) / 3;
    // Volume of a pyramid = (l * w * h) / 3 
    System.out.println("The volume inside the pyramid is: " + pyramidVolume); 
    // prints the volume of the pyramid 
    
  }//end of main method
}//end of class