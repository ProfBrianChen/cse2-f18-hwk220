//CSE 002 hw 03- Convert 
//September 15, 2018
//Wendy (HyeJi) Kim

//In this program, I'll be converting the quantity of rain into cubic miles. 
//The program will ask the user for doubles that represent the # of acres of land
//affected by hurricane precipitation and how many inches of rain were
//dropped on average. 

import java.util.Scanner; //import Scanner class

public class Convert{
  //main method required for every Java program
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in); //construct instnace of Scanner 
    
    System.out.print("Enter the affected area in acres: "); //asks user for area in acres 
    double areaInAcres = myScanner.nextDouble(); //accept user input and assign to areaInAcres
    
    System.out.print("Enter the rainfall in the affected area in inches: "); //asks user for rainfall (inches) in affected area
    double areaRainfall = myScanner.nextDouble(); //accept user input and assign it to areaRainfall
    
    double rainInGallons = areaInAcres * areaRainfall * 27154; //rainfall in acres * rainfall in inches * 27,154 gallons
    //one inch of rain on 1 acre of ground is equal to 27,154 gallons
    //converts amount of rain into gallons
    double cubicMiles = (rainInGallons * 9.08169e-13); //converts gallons into cubic miles
    System.out.println(cubicMiles + " cubic miles were affected by the rainfall."); //prints amount of rainfall in cubic miles 
    
  } //end of main method
} //end of class