// CSE 002 lab 03
// Wendy (HyeJi) Kim
// In this program, we'll be splitting a bill evenly using the Scanner class
// to obtain the original cost of the check, the % tip, and the 
// number of ways the check will be split. 
// We'll also determine how much each person needs to spend in order to pay the check.We

import java.util.Scanner; // import Scanner class

public class Check{ 
  //main method required for every Java program
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner (System.in); // construct instance of Scanner
    
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    // asks for input (original cost of check) in the form xx.xx
    double checkCost = myScanner.nextDouble(); // accept user input and assigns it to checkCost
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx) : ");
    // asks for input (% tip) as a whole number in the form xx
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; // convert the percentage into a decimal value
    
    System.out.print("Enter the number of people who went out to dinner: ");
    // asks for the number of people splitting the check
    int numPeople = myScanner.nextInt();
    // number of ways that the check will be split
    
    double totalCost;
    double costPerPerson;
    int dollars,        // whole dollar amount of cost
          dimes, pennies; // for storing digits
                // to the right of the decimal point
                // for the cost $
    totalCost = checkCost * (1 + tipPercent); // total cost including tip
    costPerPerson = totalCost / numPeople;
    // get the whole amount, dropping decimal fraction 
    dollars = (int)costPerPerson; 
    // get dimes amount, e.g.,
    // (int) (6.73 * 10) % 10 -> 67 % 10 -> 7 (remainder)
    // where the % (mod) operator returns the remainder
    // after the division: 583%100 -> 83, 27%5 -> 2
    // casted as int to truncate remaining digits 
    dimes = (int)(costPerPerson * 10) % 10;  
    pennies = (int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
  } // end of main method 
} // end of class