//CSE 002 lab 06: Display Pyramids
//Wendy (HyeJi) Kim, October 12, 2018

import java.util.Scanner;

public class PatternD{
    public static void main (String[] args){
        Scanner myScanner = new Scanner(System.in);

        //enter while loop to check the integer is within range
        System.out.print("Please input an integer between 1-10: ");
        int inputNum = myScanner.nextInt();
        while ((inputNum < 1) || (inputNum > 10)){
            System.out.print("Not within range. Please input an integer between 1-10: ");
            inputNum = myScanner.nextInt();
        }

        //the outer loop determines number of lines to print
        for(int numRows = inputNum; numRows >= 1; numRows--){
            //the inner loop determines what will be printed on each line
            for (int i = numRows; i >= 1; i--){ 
                System.out.print(i + " ");
            }
            System.out.println();
        }

    }
}