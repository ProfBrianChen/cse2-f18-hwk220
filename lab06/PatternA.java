//CSE 002 lab 06: Display Pyramids
//Wendy (HyeJi) Kim, October 12, 2018

import java.util.Scanner;

public class PatternA {
    public static void main (String[] args){
        Scanner myScanner = new Scanner(System.in);

        //enter while loop to check the integer is within range
        System.out.print("Please input an integer between 1-10: ");
        int inputNum = myScanner.nextInt();
        while ((inputNum < 1) || (inputNum > 10)){
            System.out.print("Not within range. Please input an integer between 1-10: ");
            inputNum = myScanner.nextInt();
        }

        //the outer for loop determines how many lines will be printed
        //starting at 1 row, increment row until it reaches rows indicated by inputNum
        for(int numRows = 1; numRows <= inputNum; numRows++){ 
            //the inner loop determines what will be printed on each line
            for (int i = 1; i <= numRows; i++){ //starting with 1, print, increment, and print again until it numRows equals inputNum inputted by user  
                System.out.print(i + " "); 
            }
            System.out.println();
        }

    }
}