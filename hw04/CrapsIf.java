//CSE002 hw 04- CrapsIf
//Wendy (HyeJi) Kim, September 22, 2018
//In this program, I'll ask the user if they would like to randomly cast dice or if 
//they would like to state the two dice they want to evaluate.
//If they want random, generate 2 random numbers from 1-6 (inclusive)
//If they want to choose their dice values, use the Scanner twice to ask for integers 1-6 (inclusive)
//Assume user always provides an integers. Determine the slang terminology of the outcome of the roll and print it.
//Use only if and if-else statements for this program.

import java.util.Scanner; //import Scanner class

public class CrapsIf {
  //main method required for every Java program
  public static void main (String[] args) {
  Scanner myScanner = new Scanner(System.in); //construct instance of Scanner  
    int userInt1 = 0; //declare userInt1 as an integer
    int userInt2 = 0; //declare userInt2 as an integer
    //declared outside of the if statement (important!!!)
 
    System.out.println("If you would like randomly cast dice, enter 1. If not, enter a different number."); //asks user if they'd like randomly cast dice 
    int random = myScanner.nextInt(); //accept user input 
    if (random == 1) { //if the user enters 1 and hence wants randomly cast dice 
      userInt1 = (int)(Math.random()*6)+1; //randomly output an integer between 1-6
      userInt2 = (int)(Math.random()*6)+1; //randomly output another integer between 1-6
      System.out.println("The first random number is: " + userInt1); //prints the first random integer
      System.out.println("The second random number is: " + userInt2); //prints the second random integer 
    }
      else { //if the user does not enter 1 and hence does not want randomly cast dice 
       System.out.print("Enter your first desired integer between 1-6: "); //asks user to input an integer between 1-6
       userInt1 = myScanner.nextInt(); //accepts user input and assigns to userInt1
          if ((userInt1 >=1) && (userInt1 <= 6)) { //check if number is within range of 1-6 
            System.out.println("The integers are within range."); //prints confirming the numbers are in range
          }
          else {
            System.out.println("Input not in range. Please input an integer between 1-6: "); //if input is not in range, asks user to input an integer between 1-6
            userInt1 = myScanner.nextInt(); //accepts user input, assigns to userInt1 
          }
       System.out.print("Enter your second desired integer between 1-6: "); //asks user to input a second integer between 1-6
       userInt2 = myScanner.nextInt(); //accepts user input and and assigns to userInt2
          if ((userInt2 >=1) && (userInt2 <= 6)) { //check if number is within range of 1-6 
            System.out.println("The integers are within range."); //prints confirming the numbers are in range
          }
          else {
            System.out.println("Input not in range. Please input an integer between 1-6: "); 
            userInt2 = myScanner.nextInt();
          }
     } 

    //given the exposed face of each die, assign appropriate slang terminology  
    //then print out the outcome of the roll and slang terminology
    if ((userInt1 == 1) && (userInt2 == 1)) { 
      System.out.println("The outcome of your roll is Snake Eyes.");
    }
    else if (((userInt1 == 1) && (userInt2 == 2)) || ((userInt1 == 2) && (userInt2 == 1))) {
      System.out.println("The outcome of your roll is Ace Deuce.");
    }
    else if (((userInt1 == 1) && (userInt2 == 3)) || ((userInt1 == 3) && (userInt2 == 1))) {
      System.out.println("The outcome of your roll is Easy Four.");
    }
     else if ((userInt1 == 2) && (userInt2 == 2)) {
      System.out.println("The outcome of your roll is Hard Four.");
    }
     else if (((userInt1 == 1) && (userInt2 == 4)) || ((userInt1 == 4) && (userInt2 == 1))) {
      System.out.println("The outcome of your roll is Fever Five.");
    }
     else if (((userInt1 == 2) && (userInt2 == 3)) || ((userInt1 == 3) && (userInt2 == 2))) {
      System.out.println("The outcome of your roll is Fever Five.");
    }
     else if (((userInt1 == 1) && (userInt2 == 5)) || ((userInt1 == 5) && (userInt2 == 1))) {
      System.out.println("The outcome of your roll is Easy Six.");
    }
     else if (((userInt1 == 2) && (userInt2 == 4)) || ((userInt1 == 4) && (userInt2 == 2))) {
      System.out.println("The outcome of your roll is Easy Six.");
    }
     else if ((userInt1 == 3) && (userInt2 == 3)) {
      System.out.println("The outcome of your roll is Hard Six.");
    }
     else if (((userInt1 == 1) && (userInt2 == 6)) || ((userInt1 == 6) && (userInt2 == 1))) {
      System.out.println("The outcome of your roll is Seven Out.");
    }
     else if (((userInt1 == 2) && (userInt2 == 5)) || ((userInt1 == 5) && (userInt2 == 2))) {
      System.out.println("The outcome of your roll is Seven Out.");
    }
     else if (((userInt1 == 3) && (userInt2 == 4)) || ((userInt1 == 4) && (userInt2 == 3))) {
      System.out.println("The outcome of your roll is Seven Out.");
    }
     else if (((userInt1 == 2) && (userInt2 == 6)) || ((userInt1 == 6) && (userInt2 == 2))) {
      System.out.println("The outcome of your roll is Easy Eight.");
    }
     else if (((userInt1 == 3) && (userInt2 == 5)) || ((userInt1 == 5) && (userInt2 == 3))) {
      System.out.println("The outcome of your roll is Easy Eight");
    }
     else if ((userInt1 == 4) && (userInt2 == 4)) {
      System.out.println("The outcome of your roll is Hard Eight.");
    }
     else if (((userInt1 == 3) && (userInt2 == 6)) || ((userInt1 == 6) && (userInt2 == 3))) {
      System.out.println("The outcome of your roll is Nine.");
    }
     else if (((userInt1 == 4) && (userInt2 == 5)) || ((userInt1 == 5) && (userInt2 == 4))) {
      System.out.println("The outcome of your roll is Nine.");
    }
     else if (((userInt1 == 4) && (userInt2 == 6)) || ((userInt1 == 6) && (userInt2 == 4))) {
      System.out.println("The outcome of your roll is Easy Ten.");
    }
     else if ((userInt1 == 5) && (userInt2 == 5)) {
      System.out.println("The outcome of your roll is Hard Ten.");
    }
     else if (((userInt1 == 5) && (userInt2 == 6)) || ((userInt1 == 6) && (userInt2 == 5))) {
      System.out.println("The outcome of your roll is Yo-leven.");
    }
     else if ((userInt1 == 6) && (userInt2 == 6)) {
      System.out.println("The outcome of your roll is Boxcars.");
    }
    
  } //end of main method
} //end of class