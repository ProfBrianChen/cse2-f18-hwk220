//CSE002 hw 04- CrapsSwitch
//Wendy (HyeJi) Kim, September 23, 2018
//In this program, I'll ask the user if they would like to randomly cast dice or if 
//they would like to state the two dice they want to evaluate.
//If they want random, generate 2 random numbers from 1-6 (inclusive)
//If they want to choose their dice values, use the Scanner twice to ask for integers 1-6 (inclusive)
//Assume user always provides an integers. Determine the slang terminology of the outcome of the roll and print it.
//Use only switch statements for the program.

import java.util.Scanner; //import Scanner class

public class CrapsSwitch {
  //main method required for every Java program
  public static void main (String[] args) {
  Scanner myScanner = new Scanner(System.in); //construct instance of Scanner  
    int userInt1 = 0; //declare userInt1 as an integer
    int userInt2 = 0; //declare userInt2 as an integer
    int notRandom = 0; //declare notRandom as an integer
    //declared outside of the if statement (important!!!)
  
  System.out.println("If you would like randomly cast dice, enter 1. If not, enter a different number."); //asks user if they'd like randomly cast dice 
    int random = myScanner.nextInt(); //accept user input 
    switch (random) { //switch statement if user wants random integers
      case 1: 
        userInt1 = (int)(Math.random()*6)+1; //randomly output an integer between 1-6
        userInt2 = (int)(Math.random()*6)+1; //randomly output another integer between 1-6
        System.out.println("The first random number is: " + userInt1); //prints the first random integer
        System.out.println("The second random number is: " + userInt2); //prints the second random integer 
      break; 
      default: //if random does not equal 1 aka user wants to manually input 2 integers
        notRandom = 2; 
    }
    
    switch (notRandom){ //user inputs own integers 
      case 2:
        System.out.print("Enter your first desired integer between 1-6: "); //asks user to input an integer between 1-6
        userInt1 = myScanner.nextInt(); //accepts user input and assigns to userInt1
        switch (userInt1) { 
          case 1:
          case 2:
          case 3:
          case 4:
          case 5:
          case 6:
            System.out.println("The integer is within range."); //if user inputs integer between 1-6, prints confirming it is within range  
            System.out.print("Enter your second desired integer between 1-6: "); //asks user to input the second integer between 1-6 
            userInt2 = myScanner.nextInt(); //accepts user input, assigns to userInt2
            switch (userInt2) {
              case 1:
              case 2:
              case 3:
              case 4:
              case 5:
              case 6:
                 System.out.println("The integer is within range.");
                 break;
              default:
                 System.out.print("Input not in range. Please input an integer between 1-6: ");
                 userInt2 = myScanner.nextInt();   
              }
          break;
          default:
            System.out.print("Input not in range. Please input an integer between 1-6: ");
            userInt1 = myScanner.nextInt();
            System.out.print("Enter your second desired integer between 1-6: ");
            userInt2 = myScanner.nextInt();
            switch (userInt2) {
              case 1:
              case 2:
              case 3:
              case 4:
              case 5:
              case 6:
                 System.out.println("The integer is within range."); //if user inputs integer between 1-6, prints confirming it is within range
              break;
            }
         }
    } 
     
  //switch statements to check and print slang terminology for userInt1 and userInt2
   switch (userInt1){ //for userInt1 = 1
     case 1: switch (userInt2) { //another switch statement to check userInt2 values
       case 1: 
         System.out.println("The outcome of your roll is Snake Eyes.");
         break;
       case 2:   
         System.out.println("The outcome of your roll is Ace Deuce.");
         break;
       case 3:
         System.out.println("The outcome of your roll is Easy Four.");
         break;
       case 4:
         System.out.println("The outcome of your roll is Fever Five.");
         break;
       case 5:
         System.out.println("The outcome of your roll is Easy Six.");
         break;
       case 6:
         System.out.println("The outcome of your roll is Seven Out.");
         break;  
     }
   }
   switch (userInt1){ //for userInt1 = 2
     case 2: switch (userInt2) { //another switch statement to check userInt2 values
       case 1:
         System.out.println("The outcome of your roll is Ace Deuce.");
         break;
       case 2:
         System.out.println("The outcome of your roll is Hard Four.");
         break;
       case 3:
         System.out.println("The outcome of your roll is Fever Five.");
         break;
       case 4:
         System.out.println("The outcome of your roll is Easy Six.");
         break;
       case 5:  
         System.out.println("The outcome of your roll is Seven Out.");
         break;
       case 6:
         System.out.println("The outcome of your roll is Easy Eight.");
         break;
     }
   }
   switch (userInt1){ //for userInt1 = 3
     case 3: switch(userInt2) { //another switch statement to check userInt2 values
       case 1:
         System.out.println("The outcome of your roll is Easy Four.");
         break;
       case 2:
         System.out.println("The outcome of your roll is Fever Five.");
         break;
       case 3:
         System.out.println("The outcome of your roll is Hard Six.");
         break;
       case 4:
         System.out.println("The outcome of your roll is Seven Out.");
         break;
       case 5:
         System.out.println("The outcome of your roll is Easy Eight");
         break;
       case 6:
         System.out.println("The outcome of your roll is Nine.");
         break;
     }
   }
   switch (userInt1){ //for userInt1 = 4
     case 4: switch(userInt2) { //another switch statement to check userInt2 values
       case 1: 
         System.out.println("The outcome of your roll is Fever Five.");
         break;
       case 2:
         System.out.println("The outcome of your roll is Easy Six.");
         break;
       case 3:
         System.out.println("The outcome of your roll is Seven Out.");
         break;
       case 4: 
         System.out.println("The outcome of your roll is Hard Eight.");
         break;
       case 5:
         System.out.println("The outcome of your roll is Nine.");
         break;
       case 6:
         System.out.println("The outcome of your roll is Easy Ten.");
         break;
     }
   } 
   switch (userInt1) { //for userInt1 = 5
     case 5: switch(userInt2) { //another switch statement to check userInt2 values
       case 1: 
         System.out.println("The outcome of your roll is Easy Six.");
         break;
       case 2:
         System.out.println("The outcome of your roll is Seven Out.");
         break;
       case 3:
         System.out.println("The outcome of your roll is Easy Eight");
         break;
       case 4:
         System.out.println("The outcome of your roll is Nine.");
         break;
       case 5: 
         System.out.println("The outcome of your roll is Hard Ten.");
         break;
       case 6:
         System.out.println("The outcome of your roll is Yo-leven.");
         break;
     }
   }
   switch (userInt1) { //for userInt 1 = 6
     case 6: switch(userInt2) { //another switch statement to check userInt2 values
       case 1:
         System.out.println("The outcome of your roll is Seven Out.");
         break;
       case 2:
         System.out.println("The outcome of your roll is Easy Eight.");
         break;
       case 3:
         System.out.println("The outcome of your roll is Nine.");
         break;
       case 4:
         System.out.println("The outcome of your roll is Easy Ten.");
         break;
       case 5:
         System.out.println("The outcome of your roll is Yo-leven.");
         break;
       case 6:
         System.out.println("The outcome of your roll is Boxcars.");
         break;
     }
   }
    
 
  } //end of main method
} //end of class
 