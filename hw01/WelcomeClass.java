////////////////
//// CSE 02 Welcome Message
///
public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints Welcome Message to terminal window
    System.out.println("  ----------- ");
    System.out.println("  | WELCOME | ");
    System.out.println("  ----------- ");
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-H--W--K--2--2--0->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");  
    System.out.println("  v  v  v  v  v  v");
    
  }
}
