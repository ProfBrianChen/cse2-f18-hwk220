import java.util.Random;
import java.util.Scanner;

public class Methods{
    public static void main (String[] args){
        Scanner myScanner = new Scanner(System.in);
        Random randomGenerator = new Random();
        
        System.out.println("The " + Adjectives() + " " + SubjectNoun() + " " + Verb() + " the " + ObjectNoun() + ".");
        Paragraph();
    }

    public static void Paragraph(){
        System.out.println("This " + SubjectNoun() + " was " + Adjectives() + " " + Verb() + " to " + Adjectives() + " " + SubjectNoun() + ".");
        System.out.println("It used the " + SubjectNoun() + " to " + Verb() + " " + ObjectNoun() + " at the " + Adjectives() + " " + SubjectNoun() + ".");
        System.out.println("That " + SubjectNoun() + " " + Verb() + " her " + ObjectNoun() + ".");

    }

    //method to generate random adjective
    public static String Adjectives(){
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(9);

        String adjective= "xyz";

        switch(randomInt){
            case 0:
                adjective = "frightened";
                break;
            case 1:
                adjective = "blue";
                break;
            case 2:
                adjective = "lame";
                break;
            case 3:
                adjective = "quick";
                break;
            case 4:
                adjective = "witty";
                break;
            case 5:
                adjective = "messy";
                break;
            case 6:
                adjective = "obnoxious";
                break;
            case 7:
                adjective = "curious";
                break;
            case 8:
                adjective = "helpless";
                break;
            case 9:
                adjective = "exotic";
                break;
        }
        return adjective;
    }

    //method to generate random subject noun
    public static String SubjectNoun(){
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(9);

        String subject = "xyz";
        switch(randomInt){
            case 0:
                subject = "dog";
                break;
            case 1:
                subject = "student";
                break;
            case 2:
                subject = "cow";
                break;
            case 3:
                subject = "flower";
                break;
            case 4:
                subject = "professor";
                break;
            case 5:
                subject = "cake";
                break;
            case 6:
                subject = "ice cream";
                break;
            case 7:
                subject = "chicken";
                break;
            case 8:
                subject = "class";
                break;
            case 9:
                subject = "pants";
                break;
        }
        return subject;
    }

    //method to generate random past-tense verb
    public static String Verb(){
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(9);
        
        String verb = "xyz";
        switch(randomInt){
            case 0:
                verb = "ate";
                break;
            case 1:
                verb = "passed";
                break;
            case 2:
                verb = "avoided";
                break;
            case 3:
                verb = "sparked";
                break;
            case 4: 
                verb = "trapped";
                break;
            case 5:
                verb = "spoiled";
                break;
            case 6:
                verb = "tricked";
                break;
            case 7:
                verb = "burned";
                break;
            case 8:
                verb = "bounced";
                break;
            case 9:
                verb = "arrived";
                break;
        }
        return verb;
    }

    //method to generate random object noun
    public static String ObjectNoun(){
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(9);
        
        String object = "xyz";
        switch(randomInt){
            case 0:
                object = "backyard";
                break;
            case 1:
                object = "bicycle";
                break;
            case 2:
                object = "laptop";
                break;
            case 3:
                object = "fence";
                break;
            case 4:
                object = "beanie";
                break;
            case 5:
                object = "food";
                break;
            case 6:
                object = "oven";
                break;
            case 7:
                object = "sweater";
                break;
            case 8:
                object = "waterbottle";
                break;
            case 9:
                object = "chair";
                break;
        }
        return object;
    }


}